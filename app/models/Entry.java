package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Domenovy model pro reprezentaci odpovedi v tematu
 *
 * @author Lojza
 */
@Entity
@Table(name = "entries")
public class Entry extends Model {

    public static Finder<Long, Entry> find =
            new Finder<Long, Entry>(Long.class, Entry.class);
    @Id
    private Long id;
    @Column(length = 99999)
    private String text;
    @ManyToOne
    private User author;
    @ManyToOne
    private Topic topic;
    @CreatedTimestamp
    private Date dateCreated;
    @UpdatedTimestamp
    private Date lastUpdated;

    public Entry() {
    }

    public Entry(String text, User author, Topic topic) {
        this.text = text;
        this.author = author;
        this.topic = topic;
    }

    public static Entry findByIdAndAuthorName(Long id, String authorName) {
        Entry entry = find.fetch("author").where().eq("id", id).eq("t1.username", authorName).findUnique();
        return entry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "Entry{" +
            "id=" + id +
            ", text='" + text + '\'' +
            ", author=" + author +
            ", topic=" + topic +
            ", dateCreated=" + dateCreated +
            ", lastUpdated=" + lastUpdated +
            '}';
    }
}
