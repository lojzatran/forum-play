package models;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Domenovy model pro reprezentaci uzivatelskeho profilu
 *
 * @author Lojza
 */
@Entity
@Table(name = "profiles")
public class Profile extends Model {

    public static Finder<Long, User> find = new Finder<Long, User>(
            Long.class, User.class
    );
    @Id
    @Column(name = "profile_id")
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String signature;
    private byte[] avatar;
    @OneToOne(mappedBy = "profile")
    @MapsId
    @JoinColumn(name = "profile_id")
    private User user;

    public Profile() {
    }

    public Profile(String firstName, String lastName, String email, String signature) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.signature = signature;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
