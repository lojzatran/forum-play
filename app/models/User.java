package models;

import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Domenovy model pro reprezentaci uzivatele
 *
 * @author Lojza
 */
@Entity
@Table(name = "users")
public class User extends Model {

    public static Finder<Long, User> find = new Finder<Long, User>(
            Long.class, User.class
    );
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Profile profile;
    @Enumerated(EnumType.STRING)
    private Role role;
    @OneToMany(mappedBy = "author")
    private List<Topic> myTopics;
    @OneToMany(mappedBy = "author")
    private List<Entry> myEntries;

    public static User findByUsernameAndPassword(String username, String password) {
        return find.where().eq("username", username).eq("password", password).findUnique();
    }

    public static User findByUsername(String username) {
        return StringUtils.isNotBlank(username) ? find.where().eq("username", username).findUnique() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Topic> getMyTopics() {
        return myTopics;
    }

    public void setMyTopics(List<Topic> myTopics) {
        this.myTopics = myTopics;
    }

    public List<Entry> getMyEntries() {
        return myEntries;
    }

    public void setMyEntries(List<Entry> myEntries) {
        this.myEntries = myEntries;
    }

    @PrePersist
    public void prePersist() {
        Logger.debug("akce pred ulozenim: {}", this.toString());
    }

}
