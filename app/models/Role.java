package models;

/**
 * Seznam vsech uzivatelskych roli
 *
 * @author Lojza
 */
public enum Role {
    ROLE_ADMIN, ROLE_USER, ROLE_ANONYMOUS
}
