package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Domenovy model pro reprezentaci stitku u tematu
 *
 * @author Lojza
 */
@Entity
@Table(name = "tags")
public class Tag extends Model {
    public static Finder<Long, Tag> find =
        new Finder<Long, Tag>(Long.class, Tag.class);
    @ManyToMany(mappedBy = "tagList")
    private List<Topic> topicList = new ArrayList<Topic>();
    @Id
    private Long id;
    @NotNull
    private String name;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public static Tag findByName(String name) {
        return find.where().eq("name", name).findUnique();
    }

    public List<Topic> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<Topic> topicList) {
        this.topicList = topicList;
    }

    public void removeTopic(Topic topic) {
        this.topicList.remove(topic);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Tag tag = (Tag) o;

        if (!name.equals(tag.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
