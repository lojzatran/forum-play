package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import play.Logger;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Domenovy model pro reprezentaci tematu
 *
 * @author Lojza
 */
@Entity
@Table(name = "topics")
public class Topic extends Model {

    public static Finder<Long, Topic> find =
            new Finder<Long, Topic>(Long.class, Topic.class);
    @Id
    private Long id;
    private String name;
    @Column(length = 99999)
    private String description;
    @ManyToOne
    private User author;
    @OneToMany(mappedBy = "topic")
    @OrderBy(value = "dateCreated")
    private List<Entry> entryList = new ArrayList<Entry>();
    @ManyToMany
    private List<Tag> tagList = new ArrayList<Tag>();
    @CreatedTimestamp
    private Date dateCreated;
    @UpdatedTimestamp
    private Date lastUpdated;

    public Topic() {
    }

    public Topic(String name, String description, User author) {
        this.name = name;
        this.description = description;
        this.author = author;
    }

    public static List<Topic> findByTag(String tagName) {
        List<Topic> topicList = find.fetch("tagList").where().eq("t1.name", tagName).findList();
        return topicList;
    }

    public static Topic findByIdAndAuthorName(Long id, String authorName) {
        Topic topic = find.fetch("author").where().eq("id", id).eq("t1.username", authorName).findUnique();
        return topic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public List<Entry> getEntryList() {
        return entryList;
    }

    public void setEntryList(List<Entry> entryList) {
        this.entryList = entryList;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Integer getEntriesSize() {
        return entryList.size();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Topic{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", author=" + author +
            ", entryList=" + entryList +
            ", tagList=" + tagList +
            ", dateCreated=" + dateCreated +
            ", lastUpdated=" + lastUpdated +
            '}';
    }

    @PrePersist
    public void prePersist() {
        Logger.debug("akce pred ulozenim: {}", this.toString());
    }

}
