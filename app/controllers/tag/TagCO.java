package controllers.tag;

import play.data.validation.Constraints;

/**
 * Command object pro tridu {@link models.Tag}
 *
 * @author Lojza
 */
public class TagCO {

    @Constraints.Required(message = "Toto pole musí být vyplněné!")
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
