package controllers.tag;

import models.Role;
import models.Tag;
import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import security.Security;

import java.util.Map;

/**
 * Controller pro vsechny akce tykajici se
 * tridy {@link Tag}
 *
 * @author Lojza
 */
public class TagController extends Controller {

    public static Result exists() {
        final Map<String, String[]> values = request().body().asFormUrlEncoded();
        final String tagName = values.get("tagName")[0];
        Tag tag = Tag.findByName(tagName);
        if (tag == null) {
            String username = session(Security.USERNAME_ATTR);
            User user = User.findByUsername(username);
            if (user != null) {

                if (user.getRole().equals(Role.ROLE_ADMIN)) {
                    tag = new Tag();
                    tag.setName(tagName);
                    tag.save();
                }
            }
        }
        return tag == null ? notFound("Tag neexistuje, prosím vytvořte první.") : ok(tag.getId().toString());
    }


}
