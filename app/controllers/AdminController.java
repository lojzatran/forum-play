package controllers;

import models.User;
import play.mvc.Controller;
import play.mvc.Result;
import security.SecuredAdmin;
import security.Security;
import views.html.admin.userList;

import java.util.List;

/**
 * Controller pro vsechny administratorske akce
 *
 * @author Lojza
 */
@Security.Authenticated(SecuredAdmin.class)
public class AdminController extends Controller {

    public static Result index() {
        return redirect(controllers.routes.AdminController.listUsers());
    }

    public static Result listUsers() {
        List<User> users = User.find.findList();
        return ok(userList.render(users));
    }
}
