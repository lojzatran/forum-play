package controllers.topic;

import models.Role;
import models.Tag;
import models.Topic;
import models.User;
import org.hibernate.validator.constraints.NotBlank;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * Command object pro tridu {@link Topic}
 *
 * @author Lojza
 */
public class TopicCO {

    private Long id;
    @NotBlank(message = "Toto pole musí být vyplněné!")
    private String name;
    @NotBlank
    private String description;
    private List<Tag> tagList = new ArrayList<Tag>();

    public TopicCO() {
    }

    public TopicCO(Topic topic) {
        this.id = topic.getId();
        this.description = topic.getDescription();
        this.name = topic.getName();
        this.tagList = topic.getTagList();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public void addTag(Tag tag) {
        tagList.add(tag);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Topic getTopicModel(String currentUsername, String currentRole) {
        Topic topic = null;
        if (this.id == null) {
            topic = new Topic();
            User author = User.findByUsername(currentUsername);
            topic.setAuthor(author);
        } else {
            Role role = Enum.valueOf(Role.class, currentRole);
            if (role == Role.ROLE_ADMIN) {
                topic = Topic.find.byId(this.id);
            } else {
                topic = Topic.findByIdAndAuthorName(id, currentUsername);
            }
        }
        if (topic != null) {
            topic.setName(this.name);
            topic.setDescription(this.description);
            topic.setTagList(this.tagList);
        }
        return topic;
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        List<Tag> newTagList = new ArrayList<Tag>();
        if (!this.tagList.isEmpty()) {
            for (Tag tag : this.tagList) {
                String tagName = tag.getName();
                tag = Tag.findByName(tagName);
                if (tag == null) {
                    errors.add(new ValidationError("tagList", Messages.get("tag.not.exist", tagName)));
                    break;
                } else {
                    newTagList.add(tag);
                }
            }
        }
        if (errors.isEmpty()) {
            this.tagList = newTagList;
        }
        return errors.isEmpty() ? null : errors;
    }
}
