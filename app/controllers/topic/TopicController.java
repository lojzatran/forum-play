package controllers.topic;

import models.Entry;
import models.Role;
import models.Tag;
import models.Topic;
import play.data.Form;
import play.db.ebean.Transactional;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import security.NoSecure;
import security.Secured;
import security.SecuredAdmin;
import security.Security;

import java.util.List;

/**
 * Controller pro vsechny akce tykajici se
 * tridy {@link Topic}
 *
 * @author Lojza
 */
@Security.Authenticated(NoSecure.class)
public class TopicController extends Controller {
    private static final Form<TopicCO> topicForm = Form.form(TopicCO.class);

    public static Result list() {
        return ok("list topics");
    }

    public static Result show(Long id) {
        Topic topic = Topic.find.byId(id);
        return ok(views.html.topic.show.render(topic));
    }

    @Security.Authenticated(Secured.class)
    public static Result create() {
        TopicCO topicCO = new TopicCO();
        List<Tag> tagList = Tag.find.all();
        return ok(views.html.topic.create.render(topicForm.fill(topicCO), tagList));
    }

    @Security.Authenticated(Secured.class)
    public static Result save() {
        Form<TopicCO> boundForm = topicForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            List<Tag> tagList = Tag.find.findList();
            flash("errorMessage", Messages.get("default.error.message"));
            return badRequest(views.html.topic.create.render(boundForm, tagList));
        } else {
            TopicCO topicCO = boundForm.get();
            Topic topic = topicCO.getTopicModel(session().get(Security.USERNAME_ATTR), session().get(Security.ROLE_ATTR));
            topic.save();
            flash("successMessage", Messages.get("default.save.success", topic.getName()));
            return redirect(controllers.topic.routes.TopicController.show(topic.getId()));
        }
    }

    public static Result findByTag(String tagName) {
        List<Topic> topicList = Topic.findByTag(tagName);
        return ok(views.html.topic.list.render(topicList, tagName));
    }

    @Security.Authenticated(Secured.class)
    public static Result edit(Long id) {
        Topic topic = null;
        Role role = Enum.valueOf(Role.class, session().get(Security.ROLE_ATTR));
        if (role == Role.ROLE_ADMIN) {
            topic = Topic.find.byId(id);
        } else {
            topic = Topic.findByIdAndAuthorName(id, session().get(Security.USERNAME_ATTR));
        }
        if (topic != null) {
            TopicCO topicCO = new TopicCO(topic);
            List<Tag> tagList = Tag.find.all();
            return ok(views.html.topic.edit.render(topicForm.fill(topicCO), tagList));
        } else {
            return notFound(views.html.defaultpages.notFound.render(ctx()._requestHeader(), null));
        }
    }

    @Security.Authenticated(Secured.class)
    public static Result update() {
        Form<TopicCO> boundForm = topicForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            List<Tag> tagList = Tag.find.findList();
            flash("errorMessage", Messages.get("default.error.message"));
            return badRequest(views.html.topic.edit.render(boundForm, tagList));
        } else {
            TopicCO topicCO = boundForm.get();
            Topic topic = topicCO.getTopicModel(session().get(Security.USERNAME_ATTR), session().get(Security.ROLE_ATTR));
            if (topic == null) {
                // pokud je null, tzn. ze uzivatel nema pravo upravovat dane tema
                return notFound(views.html.defaultpages.notFound.render(ctx()._requestHeader(), null));
            } else {
                topic.save();
                flash("successMessage", Messages.get("default.update.success", "Téma"));
                return redirect(controllers.topic.routes.TopicController.show(topic.getId()));
            }
        }
    }

    @Security.Authenticated(SecuredAdmin.class)
    @Transactional
    public static Result delete(Long id) {
        Topic topic = Topic.find.byId(id);
        removeAssociations(topic);
        topic.delete();
        flash("successMessage", Messages.get("default.delete.success", "Téma"));
        return redirect(controllers.routes.Application.index());
    }

    private static void removeAssociations(Topic topic) {
        List<Entry> entryList = topic.getEntryList();
        for (Entry entry : entryList) {
            entry.delete();
        }
        List<Tag> tagList = topic.getTagList();
        for (Tag tag : tagList) {
            tag.removeTopic(topic);
            tag.save();
        }
    }
}
