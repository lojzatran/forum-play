package controllers.entry;

import models.Entry;
import models.Role;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import security.NoSecure;
import security.Secured;
import security.Security;

/**
 * Controller pro vsechny akce tykajici se
 * tridy {@link Entry}
 *
 * @author Lojza
 */
@Security.Authenticated(NoSecure.class)
public class EntryController extends Controller {
    private static final Form<EntryCO> entryForm = Form.form(EntryCO.class);

    public static Result list() {
        return ok("entry list");
    }

    @Security.Authenticated(Secured.class)
    public static Result create(Long topicId) {
        return ok(views.html.entry.create.render(entryForm.fill(new EntryCO(topicId))));
    }

    @Security.Authenticated(Secured.class)
    public static Result save() {
        Form<EntryCO> boundForm = entryForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("errorMessage", Messages.get("default.error.message"));
            return badRequest(views.html.entry.create.render(boundForm));
        } else {
            EntryCO entryCo = boundForm.get();
            Entry entry = entryCo.getEntryModel(session().get(Security.USERNAME_ATTR), session().get(Security.ROLE_ATTR));
            entry.save();
            flash("successMessage", Messages.get("default.save.success", "Příspěvek"));
            return redirect(controllers.topic.routes.TopicController.show(entryCo.getTopicId()));
        }
    }

    @Security.Authenticated(Secured.class)
    public static Result edit(Long id) {
        Entry entry = null;
        Role role = Enum.valueOf(Role.class, session().get(Security.ROLE_ATTR));
        if (role == Role.ROLE_ADMIN) {
            entry = Entry.find.byId(id);
        } else {
            entry = Entry.findByIdAndAuthorName(id, session().get(Security.USERNAME_ATTR));
        }
        if (entry != null) {
            EntryCO entryCO = new EntryCO(entry);
            return ok(views.html.entry.edit.render(entryForm.fill(entryCO)));
        } else {
            return notFound(views.html.defaultpages.notFound.render(ctx()._requestHeader(), null));
        }
    }

    @Security.Authenticated(Secured.class)
    public static Result update() {
        Form<EntryCO> boundForm = entryForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("errorMessage", Messages.get("default.error.message"));
            return badRequest(views.html.entry.edit.render(boundForm));
        } else {
            EntryCO entryCO = boundForm.get();
            Entry entry = entryCO.getEntryModel(session().get(Security.USERNAME_ATTR), session().get(Security.ROLE_ATTR));
            if (entry == null) {
                // pokud je null, tzn. ze uzivatel nema pravo upravovat dany prispevek
                return notFound(views.html.defaultpages.notFound.render(ctx()._requestHeader(), null));
            } else {
                entry.save();
                flash("successMessage", Messages.get("default.update.success", "Příspěvek"));
                return redirect(controllers.topic.routes.TopicController.show(entry.getTopic().getId()));
            }
        }
    }

    @Security.Authenticated(Secured.class)
    public static Result delete(Long id) {
        Entry entry = null;
        Role role = Enum.valueOf(Role.class, session().get(Security.ROLE_ATTR));
        if (role == Role.ROLE_ADMIN) {
            entry = Entry.find.byId(id);
        } else {
            entry = Entry.findByIdAndAuthorName(id, session().get(Security.USERNAME_ATTR));
        }
        if (entry == null) {
            return notFound(views.html.defaultpages.notFound.render(ctx()._requestHeader(), null));
        } else {
            Long topicId = entry.getTopic().getId();
            entry.delete();
            flash("successMessage", Messages.get("default.delete.success", "Příspěvek"));
            return redirect(controllers.topic.routes.TopicController.show(topicId));
        }
    }
}
