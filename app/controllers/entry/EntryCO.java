package controllers.entry;

import models.Entry;
import models.Role;
import models.Topic;
import models.User;
import org.hibernate.validator.constraints.NotBlank;
import play.data.validation.Constraints;

/**
 * Command object pro tridu {@link Entry}
 *
 * @author Lojza
 */
public class EntryCO {

    private Long id;
    @NotBlank(message = "Text nesmí být prázdný!")
    private String text;
    @Constraints.Required
    private Long topicId;

    public EntryCO() {
    }

    public EntryCO(Entry entry) {
        this.id = entry.getId();
        this.text = entry.getText();
        this.topicId = entry.getTopic().getId();
    }

    public EntryCO(Long topicId) {
        this.topicId = topicId;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entry getEntryModel(String currentUsername, String currentRole) {
        Entry entry = null;
        if (this.id == null) {
            entry = new Entry();
            Topic topic = Topic.find.byId(this.topicId);
            entry.setTopic(topic);
            User author = User.findByUsername(currentUsername);
            entry.setAuthor(author);
        } else {
            Role role = Enum.valueOf(Role.class, currentRole);
            if (role == Role.ROLE_ADMIN) {
                entry = Entry.find.byId(this.id);
            } else {
                entry = Entry.findByIdAndAuthorName(id, currentUsername);
            }
        }
        if (entry != null) {
            entry.setText(this.text);
        }
        return entry;
    }
}
