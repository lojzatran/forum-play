package controllers;

import models.Role;
import models.Topic;
import models.User;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import security.NoSecure;
import security.Security;
import views.html.index;
import views.html.login;

import java.util.List;

/**
 * Vychozi controller zahrnujici spolecne akce
 */
@Security.Authenticated(NoSecure.class)
public class Application extends Controller {
    private static final Form<Login> loginForm = Form.form(Login.class);

    public static Result index() {
        List<Topic> topicList = Topic.find.findList();
        return ok(index.render(topicList));
    }

    public static Result login() {
        return ok(login.render(loginForm));
    }

    public static Result authenticate() {
        Form<Login> bindedForm = loginForm.bindFromRequest();
        if (!bindedForm.hasErrors()) {
            String username = bindedForm.field("username").value();
            String password = bindedForm.field("password").value();
            User user = User.findByUsernameAndPassword(username, password);
            if (user != null) {
                session().clear();
                session(Security.USERNAME_ATTR, user.getUsername());
                session(Security.ROLE_ATTR, user.getRole().toString());
                return redirect(routes.Application.index());
            }
        }
        return badRequest(login.render(loginForm));
    }

    public static Result logout() {
        session().clear();
        session(Security.ROLE_ATTR, Role.ROLE_ANONYMOUS.toString());
        flash("success", "Byl jste úspěšně odhlášen");
        return redirect(routes.Application.index());
    }

    public static class Login {
        @Constraints.Required
        private String username;
        @Constraints.MinLength(value = 3)
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

}
