package controllers.user;

import models.Profile;
import models.Role;
import models.User;
import org.hibernate.validator.constraints.NotBlank;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

/**
 * Command object pro prihlaseni uzivatele
 *
 * @author Lojza
 */
public class SignUpCO {

    @Constraints.ValidateWith(value = UsernameValidator.class,
            message = "Uživatelské jméno již existuje.")
    private String username;
    @Constraints.MinLength(value = 3, message = "Heslo musí mít min. 3 znaky")
    @NotBlank
    private String password;
    private String passwordAgain;
    @Constraints.Email
    @NotBlank
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = null;
        if (!this.password.equals(passwordAgain)) {
            errors = new ArrayList<ValidationError>();
            errors.add(new ValidationError("passwordAgain", "Hesla se neshodují!"));
        }
        return errors;
    }

    public User getUserModel() {
        User user = new User();
        user.setUsername(username);
        user.setRole(Role.ROLE_USER);
        user.setPassword(password);
        Profile profile = new Profile();
        profile.setEmail(email);
        user.setProfile(profile);
        return user;
    }
}
