package controllers.user;

import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import security.Secured;
import security.Security;

/**
 * Controller pro vsechny akce tykajici se
 * tridy {@link models.Profile}
 *
 * @author Lojza
 */
@Security.Authenticated(Secured.class)
public class UserProfileController extends Controller {

    private static final Form<UserProfileCO> userProfileForm = Form.form(UserProfileCO.class);

    public static Result show(String idStr) {
        Long id = null;
        if (idStr != null) {
            id = Long.parseLong(idStr);
        }
        if (id == null) {
            String currentUsername = session().get(Security.USERNAME_ATTR);
            User user = User.findByUsername(currentUsername);
            UserProfileCO userProfileCO = new UserProfileCO(user);
            return ok(views.html.user.edit.render(userProfileForm.fill(userProfileCO)));
        } else {
            User user = User.find.byId(id);
            if (user == null) {
                return notFound(views.html.defaultpages.notFound.render(ctx()._requestHeader(), null));
            }
            return ok(views.html.user.show.render(user));
        }
    }

    public static Result update() {
        return ok();
    }
}
