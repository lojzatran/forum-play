package controllers.user;

import models.Profile;
import models.User;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import play.data.validation.Constraints;

/**
 * Command object pro tridu {@link Profile}
 *
 * @author Lojza
 */
public class UserProfileCO {

    private Long id;
    private String username;
    @Constraints.MinLength(value = 3, message = "Heslo musí mít min. 3 znaky")
    private String password;
    private String passwordAgain;
    private String firstName;
    private String lastName;
    @Constraints.Email
    @NotBlank
    private String email;
    private String signature;
    private byte[] avatar;

    public UserProfileCO() {
    }

    public UserProfileCO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        Profile userProfile = user.getProfile();
        this.firstName = userProfile.getFirstName();
        this.lastName = userProfile.getLastName();
        this.email = userProfile.getEmail();
        this.signature = userProfile.getSignature();
        this.avatar = userProfile.getAvatar();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public User getUserModel(String currentUsername) {
        User user = null;
        if (currentUsername.equals(this.username)) {
            user = User.findByUsername(currentUsername);
            String password = this.getPassword();
            if (StringUtils.isNotBlank(password)) {
                user.setPassword(password);
            }
            String firstName = this.getFirstName();
            if (StringUtils.isNotBlank(firstName)) {
                user.getProfile().setFirstName(firstName);
            }
            String lastName = this.getLastName();
            if (StringUtils.isNotBlank(lastName)) {
                user.getProfile().setLastName(lastName);
            }
            String email = this.getEmail();
            if (StringUtils.isNotBlank(email)) {
                user.getProfile().setEmail(email);
            }
            String signature = this.getSignature();
            if (StringUtils.isNotBlank(signature)) {
                user.getProfile().setSignature(signature);
            }
            byte[] avatar = this.getAvatar();
            if (avatar != null && avatar.length > 0) {
                user.getProfile().setAvatar(avatar);
            }
        }
        return user;
    }
}
