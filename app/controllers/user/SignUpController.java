package controllers.user;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;
import models.User;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import security.NoSecure;
import security.Security;
import static play.Play.*;

/**
 * Controller pro vsechny akce tykajici se
 * prihlasovani
 *
 * @author Lojza
 */
@Security.Authenticated(NoSecure.class)
public class SignUpController extends Controller {
    private static final Form<SignUpCO> userForm = Form.form(SignUpCO.class);

    public static Result signUpForm() {
        SignUpCO signUpCO = new SignUpCO();
        return ok(views.html.user.signup.render(userForm.fill(signUpCO)));
    }

    public static Result signUp() {
        Form<SignUpCO> boundForm = userForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("errorMessage", Messages.get("default.error.message"));
            return badRequest(views.html.user.signup.render(boundForm));
        } else {
            SignUpCO signUpCO = boundForm.get();
            User user = signUpCO.getUserModel();
            user.save();
            sendRegistrationEmail(user);
            flash("successMessage", Messages.get("default.save.success", "Uživatel"));
            return ok(views.html.user.thankyou.render());
        }
    }

    private static void sendRegistrationEmail(User user) {
        String email = user.getProfile().getEmail();
        MailerAPI mail = application().plugin(MailerPlugin.class).email();
        mail.setSubject("Registrace");
        mail.setRecipient(user.getUsername() + " <" + email + ">");
        mail.setFrom("Forum <noreply@email.com>");
        mail.sendHtml("<html>Vítáme vás v našem diskuzním fóru.</html>");
    }
}
