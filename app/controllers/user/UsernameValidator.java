package controllers.user;

import models.User;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;
import play.libs.F;

/**
 * Vlastni validator uziv. jmena, kontroluje
 * unikatnost v databazi
 *
 * @author Lojza
 */
public class UsernameValidator extends Constraints.Validator<String> {

    @Override
    public boolean isValid(String s) {
        return !StringUtils.isBlank(s) && User.findByUsername(s) == null;
    }

    @Override
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return new F.Tuple<String, Object[]>("error.notunique.username",
                new Object[]{});
    }
}
