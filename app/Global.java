import com.avaje.ebean.Ebean;
import models.*;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.Yaml;
import play.mvc.Action;
import play.mvc.Http;
import utils.Faker;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Trida, ktera obsahuje spolecne akce pro
 * vsechny controllery
 *
 * @author Lojza
 */
public class Global extends GlobalSettings {

    @Override
    public Action onRequest(Http.Request request, Method method) {
        Logger.info("Pripojil se uzivatel z IP adresy " + request.remoteAddress() + " na adresu " + request.uri());
        return super.onRequest(request, method);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void onStart(Application app) {
        super.onStart(app);
        InitialData.insert(app);

    }

    static class InitialData {
        public static void insert(Application app) {
            if (Ebean.find(User.class).findRowCount() == 0) {
                Map<String, List<Object>> all = (Map<String, List<Object>>) Yaml.load("initial-data.yml");

                List<User> users = (List) all.get("users");
                for (User user : users) {
                    user.setProfile(new Profile("first name " + user.getUsername(), "last name",
                        user.getUsername() + "@email.com", "signature"));
                    user.save();
                }

                generateFakeTopics(users);
            }
        }

        private static void generateFakeTopics(List<User> users) {
            List<Tag> tagList = new ArrayList<Tag>();
            for (String tagName : Faker.getRandomWordsAsArray(5)) {
                Tag tag = new Tag(tagName);
                tag.save();
                tagList.add(tag);
            }
            for (int i = 0; i < 100; i++) {
                Topic topic = new Topic("Testovací téma " + i, Faker.getRandomWords(), users.get(0));
                Tag tag = tagList.get(i % 5);
                tag.getTopicList().add(topic);
                topic.getTagList().add(tag);
                topic.save();
                for (int j = 0; j < Faker.generateRandomNumberUpTo(100); j++) {
                    Entry entry = new Entry(Faker.getRandomWords(), users.get(0), topic);
                    entry.save();
                }
            }
        }
    }
}
