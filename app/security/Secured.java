package security;

import models.Role;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementace zabezpeceni pro stranky,
 * ktere jsou povolene prihlasenym uzivatelum
 *
 * @author Lojza
 */
public class Secured extends Security.Authenticator {
    @Override
    public Set<Role> getPermittedRoles() {
        Set<Role> roles = new HashSet<Role>();
        roles.add(Role.ROLE_ADMIN);
        roles.add(Role.ROLE_USER);
        return roles;
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return Results.redirect(controllers.routes.Application.login());
    }
}
