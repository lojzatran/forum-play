package security;

import models.Role;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementace zabezpeceni pro stranky,
 * ktere jsou povolene vsem
 *
 * @author Lojza
 */
public class NoSecure extends Security.Authenticator {

    @Override
    public Set<Role> getPermittedRoles() {
        Set<Role> roles = new HashSet<Role>();
        roles.add(Role.ROLE_ADMIN);
        roles.add(Role.ROLE_USER);
        roles.add(Role.ROLE_ANONYMOUS);
        return roles;
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return Results.redirect(controllers.routes.Application.login());
    }
}
