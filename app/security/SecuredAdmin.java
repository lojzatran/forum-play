package security;


import models.Role;
import play.mvc.Http;
import play.mvc.Result;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementace zabezpeceni pro stranky,
 * ktere jsou povolene administratorum
 *
 * @author Lojza
 */
public class SecuredAdmin extends Security.Authenticator {
    @Override
    public Set<Role> getPermittedRoles() {
        Set<Role> roles = new HashSet<Role>();
        roles.add(Role.ROLE_ADMIN);
        return roles;
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return notFound(views.html.defaultpages.notFound.render(ctx._requestHeader(), null));
    }
}
