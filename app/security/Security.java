package security;

import models.Role;
import play.libs.F;
import play.mvc.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;

/**
 * Hlavni trida pro rozhodovani, zda povolit ci nepovolit
 * pristup danemu uzivateli
 */
public class Security {

    public static final String USERNAME_ATTR = "username";
    public static final String ROLE_ATTR = "role";

    /**
     * Wraps the annotated action in an <code>AuthenticatedAction</code>.
     */
    @With(AuthenticatedAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Authenticated {
        Class<? extends Authenticator> value() default Authenticator.class;
    }

    /**
     * Wraps another action, allowing only authenticated HTTP requests.
     * <p/>
     * The user name is retrieved from the session cookie, and added to the HTTP request's
     * <code>username</code> attribute.
     */
    public static class AuthenticatedAction extends Action<Authenticated> {

        public F.Promise<SimpleResult> call(Http.Context ctx) {
            try {
                Authenticator authenticator = configuration.value().newInstance();
                Set<Role> permittedRoles = authenticator.getPermittedRoles();

                String roleName = authenticator.getRole(ctx);

                if (roleName == null) {
                    roleName = Role.ROLE_ANONYMOUS.toString();
                }
                Role role = Enum.valueOf(Role.class, roleName);

                if (permittedRoles.contains(role)) {
                    return delegate.call(ctx);
                } else {
                    Result unauthorized = authenticator.onUnauthorized(ctx);
                    if (unauthorized instanceof AsyncResult) {
                        return ((AsyncResult) unauthorized).getPromise();
                    } else {
                        return F.Promise.pure((SimpleResult) unauthorized);
                    }
                }

            } catch (RuntimeException e) {
                throw e;
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        }

    }

    /**
     * Handles authentication.
     */
    public abstract static class Authenticator extends Results {

      public abstract Set<Role> getPermittedRoles();

      public String getRole(Http.Context ctx) {
        return ctx.session().get(ROLE_ATTR);
      }

      /**
       * Generates an alternative result if the user is not authenticated; the default a simple '401 Not Authorized' page.
       */
      public Result onUnauthorized(Http.Context ctx) {
        return unauthorized(views.html.defaultpages.unauthorized.render());
      }
    }


}
