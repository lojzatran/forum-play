package security;

import models.Entry;
import models.Role;
import models.Topic;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Http;

/**
 * Pomocna trida pro praci s autentizaci a autorizaci
 *
 * @author Lojza
 */
public class SecurityUtils {

    public static boolean isLoggedIn(Http.Session session) {
        String username = session.get(Security.USERNAME_ATTR);
        String role = session.get(Security.ROLE_ATTR);
        Boolean isNotBlank = StringUtils.isNotBlank(username);
        Boolean isAnonymous = Role.ROLE_ANONYMOUS.toString().equals(role);
        return isNotBlank &&
                !isAnonymous;
    }

    public static boolean hasRole(Role role, Http.Session session) {
        String roleStr = session.get(Security.ROLE_ATTR);
        return StringUtils.isNotBlank(roleStr) &&
                Enum.valueOf(Role.class, roleStr).equals(role);
    }

    public static boolean canEditTopic(Topic topic, Http.Session session) {
        if (!isLoggedIn(session)) {
            return false;
        }
        Role role = Enum.valueOf(Role.class, session.get(Security.ROLE_ATTR));
        if (role == Role.ROLE_ADMIN) {
            return true;
        } else {
            String username = session.get(Security.USERNAME_ATTR);
            return topic.getAuthor().getUsername().equalsIgnoreCase(username);
        }
    }

    public static boolean canEditEntry(Entry entry, Http.Session session) {
        if (!isLoggedIn(session)) {
            return false;
        }
        Role role = Enum.valueOf(Role.class, session.get(Security.ROLE_ATTR));
        if (role == Role.ROLE_ADMIN) {
            return true;
        } else {
            String username = session.get(Security.USERNAME_ATTR);
            return entry.getAuthor().getUsername().equalsIgnoreCase(username);
        }
    }
}
