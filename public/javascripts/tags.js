require.config({
    paths: {
        tagsinput: "bootstrap-tagsinput/bootstrap-tagsinput"
    }
});

require(["bootstrap-tagsinput/bootstrap-tagsinput"], function ( tagsinput) {
    "use strict";

    $(document).ready(function () {
        $('select').on('itemRemoved', function (event) {
        });
        $('select').on('itemAdded', function (event) {
            var that = this;
            var itemName = event.item;
            $.ajax({
                url: '/tags/exists',
                type: 'POST',
                data: {
                    tagName: event.item
                },
                success: function (data) {
                    var options = $("#tagList option");
                    var lastOption = options.last()[0];
                    lastOption.value = itemName;
                    lastOption.id = 'tagList[' + (options.length - 1) + '].name';
                    console.log(data);
                },
                error: function (data) {
                    $(that).tagsinput('remove', itemName);
                }
            });
        });
    });
});