package integration;

import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.F;
import play.test.TestBrowser;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

/**
 * Test prihlasovani uzivatele do aplikace
 *
 * @author Lojza
 */
public class LoginTests {

  static final Logger LOG = LoggerFactory.getLogger(LoginTests.class);

  @Test
  public void loginAsRegularUser() {
    running(testServer(3333, fakeApplication()), HTMLUNIT, new F.Callback<TestBrowser>() {
      public void invoke(TestBrowser browser) {
        browser.goTo("http://localhost:3333/login");

        LOG.debug("Proces prihlasovani jako bezny uzivatel");
        browser.fill("input[name='username']").with("user");
        browser.fill("input[name='password']").with("user");
        browser.submit("button[type='submit']");

        FluentWebElement createTopicLink = browser.$("#create-topic-link").first();
        assertThat(createTopicLink).isNotNull();

        FluentWebElement profileLink = browser.$("#user-profile-link").first();
        assertThat(profileLink).isNotNull();

        LOG.debug("Bezny uzivatel nesmi videt link do admin menu");
        assertThat(browser.$("#admin-menu-link")).isEmpty();
      }
    });
  }

  @Test
  public void loginAsAdmin() {
    running(testServer(3333, fakeApplication()), HTMLUNIT, new F.Callback<TestBrowser>() {
      public void invoke(TestBrowser browser) {
        browser.goTo("http://localhost:3333/login");

        LOG.debug("Proces prihlasovani jako admin");
        browser.fill("input[name='username']").with("admin");
        browser.fill("input[name='password']").with("admin");
        browser.submit("button[type='submit']");

        FluentWebElement profileLink = browser.$("#user-profile-link").first();
        assertThat(profileLink).isNotNull();

        LOG.debug("U admin zkoumame, zda je videt link 'Admin menu' v hornim menu");
        assertThat(browser.$("#admin-menu-link")).isNotEmpty();
      }
    });
  }
}
