package controllers;

import com.google.common.collect.ImmutableMap;
import models.Role;
import models.User;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Result;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

/**
 * Test registrace do fora
 *
 * @author Lojza
 */
public class SignUpControllerTests {

    static final Logger LOG = LoggerFactory.getLogger(SignUpControllerTests.class);

    @Test
    public void signUp() {
        running(fakeApplication(), new Runnable() {

            @Override
            public void run() {
                LOG.debug("Simulace vytvareni uzivatele pomoci weboveho formulare");
                String username = "newUser";
                String password = "newUserPassword";
                String email = "newUser@email.com";
                Result result = callAction(
                        controllers.user.routes.ref.SignUpController.signUp(),
                        fakeRequest().withFormUrlEncodedBody(
                                ImmutableMap.of(
                                        "username", username,
                                        "password", password,
                                        "passwordAgain", password,
                                        "email", email)
                        )
                );
                assertThat(status(result))
                        .isEqualTo(OK);
                assertThat(contentAsString(result))
                        .contains("Děkujeme za registrace.");
                assertThat(flash(result).get("successMessage"))
                        .isNotEmpty();

                LOG.debug("Pro overeni najdeme uzivatele v nasi databazi");
                User newUser = User.findByUsername(username);
                assertThat(newUser.getUsername()).isEqualTo(username);
                assertThat(newUser.getPassword()).isEqualTo(password);
                assertThat(newUser.getProfile().getEmail()).isEqualTo(email);
                assertThat(newUser.getRole()).isEqualTo(Role.ROLE_USER);
            }
        });
    }

}
