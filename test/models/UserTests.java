package models;

import org.junit.Test;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

/**
 * Test vyhledavani uzivatele
 *
 * @author Lojza
 */
public class UserTests {

    @Test
    public void findByUsername() {
        running(fakeApplication(), new Runnable() {
            @Override
            public void run() {
                String userUsername = "user";
                User user = User.findByUsername(userUsername);
                assertThat(user.getUsername()).isEqualTo(userUsername);

                String adminUsername = "admin";
                User admin = User.findByUsername(adminUsername);
                assertThat(admin.getUsername()).isEqualTo(adminUsername);

                String nonExistingUsername = "        ";
                User nonExistingUser = User.findByUsername(nonExistingUsername);
                assertThat(nonExistingUser).isNull();
            }
        });
    }

    @Test
    public void findByUsernameAndPassword() {
        running(fakeApplication(), new Runnable() {
            @Override
            public void run() {
                String userUsername = "user";
                String userPassword = "user";
                User user = User.findByUsernameAndPassword(userUsername, userPassword);
                assertThat(user.getUsername()).isEqualTo(userUsername);
                assertThat(user.getPassword()).isEqualTo(userPassword);

                String adminUsername = "admin";
                String adminPassword = "admin";
                User admin = User.findByUsernameAndPassword(adminUsername, adminPassword);
                assertThat(admin.getUsername()).isEqualTo(adminUsername);
                assertThat(admin.getPassword()).isEqualTo(adminPassword);

                String adminWrongPassword = "spatneHeslo";
                admin = User.findByUsernameAndPassword(adminUsername, adminWrongPassword);
                assertThat(admin).isNull();
            }
        });
    }
}
