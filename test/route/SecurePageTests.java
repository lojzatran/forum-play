package route;

import models.Role;
import org.junit.Test;
import play.mvc.Result;
import security.Security;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

/**
 * Test zabezpeceni aplikace
 *
 * @author Lojza
 */
public class SecurePageTests {

    @Test
    public void authenticationFailure() {
        running(fakeApplication(), new Runnable() {

            @Override
            public void run() {
                Result result = route(fakeRequest(GET, "/topics/create"));

                assertThat(status(result)).isEqualTo(SEE_OTHER);
                assertThat(redirectLocation(result)).isEqualTo("/login");
            }
        });
    }

    @Test
    public void goToSecurePageAsAuthUser() {
        running(fakeApplication(), new Runnable() {
            @Override
            public void run() {
                Result result = route(fakeRequest(GET, "/topics/create").withSession(Security.USERNAME_ATTR, "user").
                        withSession(Security.ROLE_ATTR, Role.ROLE_USER.toString()));

                assertThat(status(result)).isEqualTo(OK);
                assertThat("text/html").isEqualTo(contentType(result));
                String content = contentAsString(result);
                assertThat(content).contains("Nové téma");
            }
        });
    }
}
