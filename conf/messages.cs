forum.brand=Fórum
default.save.success={0} byl úspěšně vytvořen.
default.update.success={0} byl úspěšně změněn.
default.delete.success={0} byl úspěšně smazán.
default.error.message=Máte chybu.

# Log in
login.label=Přihlásit se
login.username=Jméno
login.password=Heslo

# Topic
topic.list=Seznam témat
topic.new=Nové téma
topic.answer=Odpovědět
topic.not.exist=Dané téma neexistuje!

# Entry
entry.new=Nový příspěvek

# Tag
tag.not.exist=Zadaný tag {0} neexistuje!

# User
error.notunique.username=Uživatelské jméno je obsazené!