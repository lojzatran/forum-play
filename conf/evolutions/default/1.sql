# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table entries (
  id                        bigint not null,
  text                      varchar(99999),
  author_id                 bigint,
  topic_id                  bigint,
  date_created              timestamp not null,
  last_updated              timestamp not null,
  constraint pk_entries primary key (id))
;

create table profiles (
  profile_id                bigint not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  email                     varchar(255),
  signature                 varchar(255),
  avatar                    varbinary(255),
  constraint pk_profiles primary key (profile_id))
;

create table tags (
  id                        bigint not null,
  name                      varchar(255) not null,
  constraint pk_tags primary key (id))
;

create table topics (
  id                        bigint not null,
  name                      varchar(255),
  description               varchar(99999),
  author_id                 bigint,
  date_created              timestamp not null,
  last_updated              timestamp not null,
  constraint pk_topics primary key (id))
;

create table users (
  id                        bigint not null,
  username                  varchar(255),
  password                  varchar(255),
  profile_profile_id        bigint,
  role                      varchar(14),
  constraint ck_users_role check (role in ('ROLE_ADMIN','ROLE_USER','ROLE_ANONYMOUS')),
  constraint uq_users_username unique (username),
  constraint pk_users primary key (id))
;


create table topics_tags (
  topics_id                      bigint not null,
  tags_id                        bigint not null,
  constraint pk_topics_tags primary key (topics_id, tags_id))
;
create sequence entries_seq;

create sequence profiles_seq;

create sequence tags_seq;

create sequence topics_seq;

create sequence users_seq;

alter table entries add constraint fk_entries_author_1 foreign key (author_id) references users (id) on delete restrict on update restrict;
create index ix_entries_author_1 on entries (author_id);
alter table entries add constraint fk_entries_topic_2 foreign key (topic_id) references topics (id) on delete restrict on update restrict;
create index ix_entries_topic_2 on entries (topic_id);
alter table topics add constraint fk_topics_author_3 foreign key (author_id) references users (id) on delete restrict on update restrict;
create index ix_topics_author_3 on topics (author_id);
alter table users add constraint fk_users_profile_4 foreign key (profile_profile_id) references profiles (profile_id) on delete restrict on update restrict;
create index ix_users_profile_4 on users (profile_profile_id);



alter table topics_tags add constraint fk_topics_tags_topics_01 foreign key (topics_id) references topics (id) on delete restrict on update restrict;

alter table topics_tags add constraint fk_topics_tags_tags_02 foreign key (tags_id) references tags (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists entries;

drop table if exists profiles;

drop table if exists tags;

drop table if exists topics_tags;

drop table if exists topics;

drop table if exists users;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists entries_seq;

drop sequence if exists profiles_seq;

drop sequence if exists tags_seq;

drop sequence if exists topics_seq;

drop sequence if exists users_seq;

